# DEMO Springboot application (Jar)

## prerequisite
* Maven 3.6.3
* JDK 14

## Edit Body

 You can edit web body in src/main/java/com/marcotechnology/springbootdemojar/SpringbootdemowarController.java

 ## Edit Port

 You can edit port at  src/main/java/com/marcotechnology/springbootdemojar/resource/application.properties
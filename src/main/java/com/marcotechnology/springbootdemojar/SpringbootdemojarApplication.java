package com.marcotechnology.springbootdemojar;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringbootdemojarApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringbootdemojarApplication.class, args);
	}

}
